﻿using Assets.Scripts.GameControl;
using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.Move
{
    internal class Movement
    {
        protected int DirectionCoefficient;
        private float speed;
        private Transform characterTransform;

        public Movement(Transform transform, float speed, MovementDirection direction)
        {
            characterTransform = transform;
            this.speed = speed;

            if (direction == MovementDirection.Right)
                DirectionCoefficient = 1;
            else
                DirectionCoefficient = -1;

            PlayerController.OnEnemyContact += Stop;
            GameController.OnUpdate += Move;
            GameController.OnSceneFinished += Unsubscribe;
        }

        private void Unsubscribe()
        {
            PlayerController.OnEnemyContact -= Stop;
            GameController.OnUpdate -= Move;
            GameController.OnSceneFinished -= Unsubscribe;
        }

        private void Move()
        {
            characterTransform.Translate(DirectionCoefficient * Vector2.right * speed * Time.deltaTime);
        }

        private void Stop()
        {
            GameController.OnUpdate -= Move;
        }
    }
}
