﻿using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.Move
{
    internal class PlayerMovement : Movement
    {
        public PlayerMovement(Transform transform, float speed, MovementDirection direction) : base(transform, speed, direction)
        {
            PlayerShooting.OnShootingEvent += PlayerStop;
        }

        private void PlayerStop(bool isStop)
        {
            if (isStop)
                DirectionCoefficient = 0;
            else
                DirectionCoefficient = 1;
        }
    }
}
