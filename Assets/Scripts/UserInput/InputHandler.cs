﻿using Assets.Scripts.GameControl;
using System;
using UnityEngine;

namespace Assets.Scripts.UserInput
{
    internal class InputHandler
    {
        public static event Action OnMouseButtonDown;
        
        public InputHandler()
        {
            GameController.OnUpdate += MouseButtonInput;
            GameController.OnSceneFinished += Unsubscribe;
        }

        private void Unsubscribe()
        {
            GameController.OnUpdate -= MouseButtonInput;
            GameController.OnSceneFinished -= Unsubscribe;
        }

        private void MouseButtonInput()
        {
            if (Input.GetMouseButton(0))
                OnMouseButtonDown?.Invoke();
        }
    }
}
