﻿using Assets.Scripts.GameControl;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    internal class Explosion
    {
        private Transform explosionPoint;
        private ParticleSystem explosionParticleSystem;

        public Explosion(Transform transform) 
        {
            explosionPoint = transform;
            explosionParticleSystem = Resources.Load<ParticleSystem>("ExplosionParticleSystem");

            EnemyRespawnHandler.OnExplosion += ActivateExplosionEffect;
            GameController.OnSceneFinished += Unsubscribe;
        }

        private void Unsubscribe()
        {
            EnemyRespawnHandler.OnExplosion -= ActivateExplosionEffect;
            GameController.OnSceneFinished -= Unsubscribe;
        }

        public void ActivateExplosionEffect()
        {
            Object.Instantiate(explosionParticleSystem, explosionPoint.position, Quaternion.identity);
        }
    }
}
