﻿using Assets.Scripts.GameControl;
using System;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    internal class EnemyRespawnHandler
    {
        public static event Action OnExplosion;
        
        private Transform enemyTransform;
        private Transform respawnPoint;
        private Timer timer;
        
        public EnemyRespawnHandler(Transform enemy, Transform respawn)
        {
            enemyTransform = enemy;
            respawnPoint = respawn;
        }

        public void Start()
        {
            DelayTimer(true);
        }

        private void Respawn()
        {
            OnExplosion.Invoke();
            enemyTransform.position = respawnPoint.position;
            DelayTimer(false);
        }

        private void DelayTimer(bool isRun)
        {
            float delay = 0.6f;
            
            if (isRun)
            {
                timer = new Timer();
                timer.Start(delay);
                timer.OnTimerFinished += Respawn;
            }
            else
            {
                timer.OnTimerFinished -= Respawn;
                timer = null;
            }
        }
    }
}
