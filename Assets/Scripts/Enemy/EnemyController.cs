﻿using Assets.Scripts.Move;
using Spine.Unity;
using System;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    internal class EnemyController : MonoBehaviour
    {
        public static event Action OnHit;
        
        [SerializeField] private Transform explosionPoint;
        [SerializeField] private Transform respawnPoint;
        
        private EnemyRespawnHandler enemyRespawn;

        private float speed = 2.5f;

        private void Awake()
        {
            var animationState = GetComponentInChildren<SkeletonAnimation>().AnimationState;

            new Movement(transform, speed, MovementDirection.Left);
            new EnemyAnimationHandler(animationState);
            new Explosion(explosionPoint);
            enemyRespawn = new EnemyRespawnHandler(transform, respawnPoint);
        }

        private void OnMouseDown()
        {
            enemyRespawn.Start();
        }
    }
}
