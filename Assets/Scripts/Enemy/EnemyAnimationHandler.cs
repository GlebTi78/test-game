﻿using Assets.Scripts.GameControl;

namespace Assets.Scripts.Enemy
{
    internal class EnemyAnimationHandler
    {
        private Spine.AnimationState animationState;
        private Timer angryAnimationTimer;
        public EnemyAnimationHandler(Spine.AnimationState animationState) 
        {
            this.animationState = animationState;

            angryAnimationTimer = new Timer();
            angryAnimationTimer.OnTimerFinished += AngryAnimation;

            GameController.OnPlayerLost += WinAnimation;
            GameController.OnSceneFinished += Unsubscribe;

            animationState.SetAnimation(0, "run", true);
            AngryAnimation();
        }

        private void Unsubscribe()
        {
            angryAnimationTimer.OnTimerFinished -= AngryAnimation;
            GameController.OnPlayerLost -= WinAnimation;
            GameController.OnSceneFinished -= Unsubscribe;
        }

        private void WinAnimation()
        {
            Dispose();
            animationState.SetAnimation(0, "win", true);
        }

        private void AngryAnimation()
        {
            float angryAnimationDelay = 2.5f;
            animationState.SetAnimation(1, "angry", false);
            angryAnimationTimer.Start(angryAnimationDelay);
        }

        private void Dispose()
        {
            animationState.ClearTracks();
            Unsubscribe();
        }
    }
}
