﻿using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.Sound
{
    internal class SoundController : MonoBehaviour
    {
        [SerializeField] private AudioSource musicAudioSource;
        [SerializeField] private AudioSource effectsAudioSource;

        private void Awake()
        {
            PlayerShooting.OnShot += PlayShotSound;
        }

        private void OnDestroy()
        {
            PlayerShooting.OnShot -= PlayShotSound;
        }

        private void PlayShotSound()
        {
            effectsAudioSource.Play();
        }
    }
}
