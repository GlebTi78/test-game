﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.GameControl;

namespace Assets.Scripts.UI
{
    internal class UIController : MonoBehaviour
    {
        [SerializeField] private GameObject resultGamePanel;
        [SerializeField] private Button exitButton;
        [SerializeField] private Text resultGameText;
        
        private void Awake()
        {
            resultGamePanel.SetActive(false);
            exitButton.onClick.AddListener(Exit);
            GameController.OnGameEnded += ResultGamePanelOpen;
        }

        private void OnDestroy()
        {
            exitButton.onClick.RemoveAllListeners();
            GameController.OnGameEnded -= ResultGamePanelOpen;
        }

        private void ResultGamePanelOpen(GameResult result)
        {
            if (result == GameResult.Win)
                resultGameText.text = "Уровень пройден!";
            else
                resultGameText.text = "Вы проиграли.";

            resultGamePanel.SetActive(true);
        }

        private void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}
