﻿using Assets.Scripts.Enemy;
using Assets.Scripts.Move;
using Assets.Scripts.UserInput;
using Spine.Unity;
using System;
using UnityEngine;

namespace Assets.Scripts.Player
{
    internal class PlayerController : MonoBehaviour
    {
        public static event Action OnEnemyContact;
        
        [SerializeField] private float speed;
        [SerializeField] private ParticleSystem shotParticleSystem;

        private void Awake()
        {
            var animationState = GetComponentInChildren<SkeletonAnimation>().AnimationState;

            new InputHandler();
            new PlayerMovement(transform, speed, MovementDirection.Right);
            new PlayerAnimationHandler(animationState);
            new PlayerShooting(shotParticleSystem);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent<EnemyController>(out EnemyController enemyController))
            {
                OnEnemyContact.Invoke();
            }
        }
    }
}
