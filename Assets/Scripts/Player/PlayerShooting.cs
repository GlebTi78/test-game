﻿using Assets.Scripts.GameControl;
using Assets.Scripts.UserInput;
using System;
using UnityEngine;

namespace Assets.Scripts.Player
{
    internal class PlayerShooting
    {
        public static event Action OnShot;
        public static event Action<bool> OnShootingEvent;

        private ParticleSystem shotParticleSystem;
        private Timer particleDelayTimer;
        private Timer reloadingTimer;
        private bool isCharged = true;

        public PlayerShooting(ParticleSystem particleSystem)
        {
            shotParticleSystem = particleSystem;
            
            InputHandler.OnMouseButtonDown += Shot;
            GameController.OnPlayerLost += Unsubscribe;
            GameController.OnSceneFinished += Unsubscribe;
        }

        private void Unsubscribe()
        {
            InputHandler.OnMouseButtonDown -= Shot;
            GameController.OnPlayerLost -= Unsubscribe;
            GameController.OnSceneFinished -= Unsubscribe;
        }

        private void Shot()
        {
            if (isCharged)
            {
                OnShootingEvent.Invoke(true);
                OnShot.Invoke();
                ParticleDelayTimer(true);
                isCharged = false;
                ReloadingTimer(true);
            }
        }

        private void ParticleSystemStart()
        {
            shotParticleSystem.Play();
            ParticleDelayTimer(false);
        }

        private void Reloading()
        {
            OnShootingEvent.Invoke(false);
            isCharged = true;
            ReloadingTimer(false);
        }
        
        private void ParticleDelayTimer(bool isRun)
        {
            float delay = 0.6f;
            
            if (isRun)
            {
                particleDelayTimer = new Timer();
                particleDelayTimer.OnTimerFinished += ParticleSystemStart;
                particleDelayTimer.Start(delay);
            }
            else
            {
                particleDelayTimer.OnTimerFinished -= ParticleSystemStart;
            }
        }

        private void ReloadingTimer(bool isRun)
        {
            float reloadingTime = 4.55f;

            if (isRun)
            {
                reloadingTimer = new Timer();
                reloadingTimer.OnTimerFinished += Reloading;
                reloadingTimer.Start(reloadingTime);
            }
            else
            {
                reloadingTimer.OnTimerFinished -= Reloading;
            }
        }
    }
}
