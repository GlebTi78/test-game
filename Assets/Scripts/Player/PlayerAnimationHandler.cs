﻿using Assets.Scripts.GameControl;
using Spine;

namespace Assets.Scripts.Player
{
    internal class PlayerAnimationHandler
    {
        private Spine.AnimationState animationState;

        public PlayerAnimationHandler(Spine.AnimationState animationState)
        {
            this.animationState = animationState;
            animationState.SetAnimation(0, "walk", true);

            PlayerShooting.OnShot += Shot;
            PlayerController.OnEnemyContact += LooseAnimation;
            GameController.OnSceneFinished += Unsubscribe;
        }

        private void Unsubscribe()
        {
            PlayerShooting.OnShot -= Shot;
            PlayerController.OnEnemyContact -= LooseAnimation;
            GameController.OnSceneFinished -= Unsubscribe;
        }
        
        private void Shot()
        {
            animationState.ClearTracks();
            animationState.AddAnimation(0, "shoot", false, 0);
            animationState.AddAnimation(0, "shoot_fail", false, 0);
            animationState.AddAnimation(0, "walk", true, 0);
        }

        private void LooseAnimation()
        {
            animationState.ClearTracks();
            animationState.SetAnimation(0, "loose", false);
        }
    }
}
