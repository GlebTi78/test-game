﻿namespace Assets.Scripts.GameControl
{
    internal enum GameResult
    {
        Win,
        Lose
    }
}
