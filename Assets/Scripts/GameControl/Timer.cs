﻿using System;

namespace Assets.Scripts.GameControl
{
    internal class Timer
    {
        public event Action OnTimerFinished;

        private float duration;
        private float tickValue = 0.02f;

        public void Start(float duration)
        {
            this.duration = duration;
            GameController.OnFixedUpdate += UpdateTime;
        }

        private void UpdateTime()
        {
            duration -= tickValue;

            if (duration <= 0)
            {
                OnTimerFinished?.Invoke();
                GameController.OnFixedUpdate -= UpdateTime;
            }
        }
    }
}
