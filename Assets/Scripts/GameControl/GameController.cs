﻿using Assets.Scripts.Player;
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.GameControl
{
    internal class GameController : MonoBehaviour
    {
        public static event Action OnUpdate;
        public static event Action OnFixedUpdate;
        public static event Action OnSceneFinished;
        public static event Action OnPlayerLost;
        public static event Action<GameResult> OnGameEnded;

        [SerializeField] GameObject enemy;

        private void Awake()
        {
            PlayerController.OnEnemyContact += PlayerLost;
            EndLevelTriggerController.OnLevelEnded += PlayerWin;
        }

        private void OnDestroy()
        {
            OnSceneFinished.Invoke();
            PlayerController.OnEnemyContact -= PlayerLost;
            EndLevelTriggerController.OnLevelEnded -= PlayerWin;
        }

        private void Update()
        {
            OnUpdate?.Invoke();
        }

        private void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }

        private void PlayerWin()
        {
            enemy.SetActive(false);
            StartCoroutine(EndGameViaPause(GameResult.Win));
        }
        
        private void PlayerLost()
        {
            OnPlayerLost.Invoke();
            StartCoroutine(EndGameViaPause(GameResult.Lose));
        }

        private IEnumerator EndGameViaPause(GameResult result)
        {
            yield return new WaitForSeconds(3);
            OnGameEnded.Invoke(result);
        }
    }
}
