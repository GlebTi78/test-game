﻿using Assets.Scripts.Player;
using System;
using UnityEngine;

namespace Assets.Scripts.GameControl
{
    internal class EndLevelTriggerController : MonoBehaviour
    {
        public static event Action OnLevelEnded;
        
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent<PlayerController>(out PlayerController player))
            {
                OnLevelEnded.Invoke();
            }
        }
    }
}
